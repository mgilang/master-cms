var user = {
	edit: function(elem){
		id = $(elem).data("id");
		$.ajax({
			type : "get",
			dataType: "json",
			url : base_url+"/user/"+id,
			success : function(result){
				var form = $("#edit-form");

				form.attr("action", base_url+"/user/edit/"+result.id);
				$("#edit-name").val(result.name);
				$("#edit-phone_number").val(result.phone_number);
				$("#edit-email").val(result.email);
				$("#edit-address").html(result.address);

				$('#edit-modal').modal();
			}
		});

	}
};

$(document).ready(function () {
    $(".edit-user").click(function(){
		user.edit(this);
	});
	
});

