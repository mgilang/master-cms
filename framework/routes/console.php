<?php

use Illuminate\Foundation\Inspiring;
use App\AdCampaign;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

//publish campaign
Artisan::command('publish_campaign', function () {
    //cek folder 
    if(!is_dir(public_path('indicators'))){
        mkdir(public_path('indicators'), 0755, true);
        
    }

    // cek file indicator
    if(file_exists(public_path('indicators/publish_campaign.indicator'))){
        exit();
    }

    //create file indicator
    file_put_contents(public_path('indicators/publish_campaign.indicator'), 1);

    $campaigns = AdCampaign::where("expired_date", NULL)->get();
    $counter = 0;

    foreach($campaigns as $data){
        if($data->active_date == date('Y-m-d') && $data->moderation_status == 0){
            //activate campaign
            $data->expired_date = date('Y-m-d', strtotime("+30 day ".$data->active_date));
            $data->campaign_status = 1;

            $data->save();
            $counter++;
        }


    }
    //remove indicator
    unlink(public_path('indicators/publish_campaign.indicator'));
    $this->comment($counter." campaign published");
})->describe('Publish Scheduled Campaign');
