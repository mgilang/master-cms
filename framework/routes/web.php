<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function(){
	return redirect('/dashboard');
});

Auth::routes();

//dashboard
Route::get('dashboard', 'DashboardController@index')->middleware('auth');

//user management
Route::get('/users', 'UserController@index')->middleware('auth');
Route::get('/user/{id}', 'UserController@show')->middleware('auth');
Route::put('/user/edit/{id}', 'UserController@update')->middleware('auth');
Route::post('/user', 'UserController@store')->middleware('auth');
//soft delete
Route::delete('/user/delete/{id}', 'UserController@destroy')->middleware('auth');
Route::get('/user/set-active/{id}', 'UserController@setActive')->middleware('auth');